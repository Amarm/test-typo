$(document).ready(function() {
  var isBackgroundWhite = true;
    $(".switch-btn" ).click(function() {
        let body = $("body");
        let bandName = $(".bandName")
        let bgColor = isBackgroundWhite ? "black" : "white";
        let textColor = isBackgroundWhite ? "white" : "black";
        body.css("background-color", bgColor);
        bandName.css("color", textColor);
        isBackgroundWhite = !isBackgroundWhite;
      });

      $(".spacing-btn" ).click(function(event) {
        let target = $(event.target);
        let letterSpacing = parseInt($(".bandName").css("letter-spacing"));
        if (target.hasClass("plus")) $(".bandName").css("letter-spacing", `${letterSpacing += 2}px`);
        if (target.hasClass("minus")) $(".bandName").css("letter-spacing", `${letterSpacing -= 2}px`);
        console.log(target)
      });
    
});